<?php

	get_header();

	get_template_part('template-parts/section', 'hero');
	get_template_part('template-parts/section', 'breadcrumb');
	get_template_part('template-parts/section', 'builder');
	get_template_part('template-parts/section', 'cta');
	get_template_part('template-parts/section', 'logoslider');

	get_footer();
