<?php
//******************************************************************************
// Empty search request
function empty_search_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_filter( 'request', 'empty_search_filter' );
?>
