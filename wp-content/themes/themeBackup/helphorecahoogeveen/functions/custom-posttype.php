<?php
//******************************************************************************
// Register Custom post types
function vm_register_post_type()
{
	// Register Custom Objecten
	$labels = array(
		'name'           => _x( 'Logos', 'Logos General Name', 'helphorecaHoogeveen' ),
		'singular_name'  => _x( 'Logo', 'Logo Singular Name', 'helphorecaHoogeveen' ),
		'menu_name'      => __( 'Logos', 'helphorecaHoogeveen' ),
		'name_admin_bar' => __( 'Logo', 'helphorecaHoogeveen' ),
		'archives'       => __( 'Logos archief', 'helphorecaHoogeveen' ),
		'all_items'      => __( 'Alle Logos', 'helphorecaHoogeveen' ),
		'add_new_item'   => __( 'Nieuwe Logo toevoegen', 'helphorecaHoogeveen' ),
		'add_new'        => __( 'Nieuwe Logo', 'helphorecaHoogeveen' ),
		'new_item'       => __( 'Nieuwe Logo', 'helphorecaHoogeveen' ),
		'edit_item'      => __( 'Logo aanpassen', 'helphorecaHoogeveen' ),
		'update_item'    => __( 'Logo updaten', 'helphorecaHoogeveen' ),
		'view_item'      => __( 'Logo weergeven', 'helphorecaHoogeveen' ),
		'view_items'     => __( 'Logos weergeven', 'helphorecaHoogeveen' ),
	);
	$args   = array(
		'label'               => __( 'Logo', 'text_domain' ),
		'description'         => __( 'Omschrijving Logos', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 25,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'menu_icon'           => 'dashicons-images-alt',
	);

	register_post_type( 'logos', $args );
}

add_action( 'init', 'vm_register_post_type', 0 );

function dappr_helphorecahoogeveen_loterijprijzen()
{
	// Register Custom Objecten
	$labels = array(
		'name'           => _x( 'Loterij prijzen', 'Loterij prijzen General Name', 'helphorecaHoogeveen' ),
		'singular_name'  => _x( 'Loterij prijs', 'Loterij prijs Singular Name', 'helphorecaHoogeveen' ),
		'menu_name'      => __( 'Loterij prijzen', 'helphorecaHoogeveen' ),
		'name_admin_bar' => __( 'Loterij prijs', 'helphorecaHoogeveen' ),
		'archives'       => __( 'Loterij prijzen archief', 'helphorecaHoogeveen' ),
		'all_items'      => __( 'Alle Loterij prijzen', 'helphorecaHoogeveen' ),
		'add_new_item'   => __( 'Nieuwe Loterij prijs toevoegen', 'helphorecaHoogeveen' ),
		'add_new'        => __( 'Nieuwe Loterij prijs', 'helphorecaHoogeveen' ),
		'new_item'       => __( 'Nieuwe Loterij prijs', 'helphorecaHoogeveen' ),
		'edit_item'      => __( 'Loterij prijs aanpassen', 'helphorecaHoogeveen' ),
		'update_item'    => __( 'Loterij prijs updaten', 'helphorecaHoogeveen' ),
		'view_item'      => __( 'Loterij prijs weergeven', 'helphorecaHoogeveen' ),
		'view_items'     => __( 'Loterij prijzen weergeven', 'helphorecaHoogeveen' ),
	);
	$args   = array(
		'label'               => __( 'Loterij prijs', 'text_domain' ),
		'description'         => __( 'Omschrijving Loterij prijzen', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 25,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'menu_icon'           => 'dashicons-tickets',
	);

	register_post_type( 'loterijprijzen', $args );
}

add_action( 'init', 'dappr_helphorecahoogeveen_loterijprijzen', 0 );
