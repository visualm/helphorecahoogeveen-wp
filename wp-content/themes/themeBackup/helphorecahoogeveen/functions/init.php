<?php
 if (is_admin()){
        $new_page_title = 'Home';
        $new_page_template = '';

        $page_check = get_page_by_title($new_page_title);
        $new_page = array(
                'post_type' 		=> 'page',
                'post_title' 		=> $new_page_title,
                'post_status' 		=> 'publish',
                'post_author' 		=> 1,
				'order'  		=> 1,
        );
        if(!isset($page_check->ID)){
                $new_page_id = wp_insert_post($new_page);
                if(!empty($new_page_template)){
                        update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
                }
        }
}

if (is_admin()){
	   $new_page_title = 'Contact';
	   $new_page_template = 'Contact';

	   $page_check = get_page_by_title($new_page_title);
	   $new_page = array(
			   'post_type' 			=> 'page',
			   'post_title' 		=> $new_page_title,
			   'post_status' 		=> 'publish',
			   'post_author' 		=> 1,
			   'order'  		=> 100,
	   );
	   if(!isset($page_check->ID)){
			   $new_page_id = wp_insert_post($new_page);
			   if(!empty($new_page_template)){
					   update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
			   }
	   }
}
add_theme_support( 'post-thumbnails', array( 'post', 'page', 'movie', 'product' ) );
?>
