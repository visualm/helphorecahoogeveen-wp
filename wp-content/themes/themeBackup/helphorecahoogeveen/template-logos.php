<?php

	/* Template Name: Logos */

	get_header();

	get_template_part('template-parts/section', 'hero');
	get_template_part('template-parts/section', 'breadcrumb');
	get_template_part('template-parts/section', 'logos');
	
	get_footer();
