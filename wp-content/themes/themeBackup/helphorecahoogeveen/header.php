<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<title><?php wp_title(''); ?></title>
	<!-- Meta -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="author" content="<?=get_bloginfo('name');?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Link -->
	<style>
		<?= include 'dist/css/style.critical.css';?>
	</style>
	<link rel="stylesheet" href="<?=autoVer('/dist/css/style.css');?>">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">

	<!-- Scripts -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	
	<?php get_template_part('template-parts/nav', 'desktop'); ?>
	<?php get_template_part('template-parts/nav', 'mobile'); ?>
