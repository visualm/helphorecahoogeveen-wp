<span id="formscroll"></span>
<form class="dynamic-ajax-form contact" action="<?=get_template_directory_uri();?>/mail/send-contact.php" method="post">
	<div class="form__row">
		<div class="form__field--half">
			<input class="required" type="text" name="name" value="" placeholder="<?=__('Contactpersoon', 'VisualMedia');?>*...">
		</div>
		<div class="form__field--half">
			<input class="required" type="text" name="company" value="" placeholder="<?=__('Bedrijfsnaam', 'VisualMedia');?>*...">
		</div>
	</div>

	<div class="form__row">
		<div class="form__field--half">
			<input class="required" type="email" name="email" value="" placeholder="<?=__('E-mailadres', 'VisualMedia');?>*...">
		</div>
		<div class="form__field--half">
			<input class="required" type="tel" name="phone" value="" placeholder="<?=__('Telefoonnummer', 'VisualMedia');?>*...">
		</div>
	</div>

	<div class="form__row">
		<div class="form__field">
			<textarea class="required" name="message" rows="8" cols="80" placeholder="<?=__('Typ hier je bericht', 'VisualMedia');?>*..."></textarea>
		</div>
	</div>

	<div class="form__row">
		<div class="form__field--half">
			<button name="button" class="form-submit" id="send" class="button"><?=__('Bericht verzenden', 'VisualMedia');?></button>
		</div>
	</div>

	<input type="hidden" name="vacature" value="<?=the_title();?>">

</form>


<div id="contact_results"></div>
