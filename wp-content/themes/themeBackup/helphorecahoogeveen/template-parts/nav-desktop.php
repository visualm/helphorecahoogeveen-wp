<header class="header desktop">


		<?php if (get_field('logo_dark', 'options')): ?>
			<a href="<?=get_home_url();?>" class="logo logo-klein">
				<?php $logo_id_small = get_field('logo_dark', 'options');?>
				<img src="<?= wp_get_attachment_image_url($logo_id_small, 'full');?>" alt="logo">
			</a>
		<?php endif; ?>

		<div class="inner">
			<nav class="nav-desktop">
				<?php wp_nav_menu( array(
					'theme_location' => 'main-menu'
				)); ?>
			</nav>
		</div>

</header>
