<section class="s-hero-big">
		<div id="slider-hero">
			<?php if ( have_rows( 'slider' ) ) : ?>
				<?php while ( have_rows( 'slider' ) ) : the_row(); ?>
					<?php $slider_bg = get_sub_field( 'slider-bg' ); ?>

					<div class="slide" style="background-image: url('<?=wp_get_attachment_image_url( $slider_bg, 'full' ); ?>');">
						<div class="container">
							<div class="inner">
								<p>
									<?=the_sub_field( 'slider-content' ); ?>
									<a href="<?=the_sub_field( 'slider-url' ); ?>" class="button--light has-icon"><?=the_sub_field( 'slider-url-text' ); ?> <i class="fas fa-chevron-right"></i></a>
								</p>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php else : ?>
				<?php $hero_fallback = get_field( 'hero_fallback', 'option' ); ?>

				<?php if ( $hero_fallback ) : ?>
					<div class="slide fallback" style="background-image: url('<?=wp_get_attachment_image_url( $hero_fallback, 'full' ); ?>');">
				<?php endif; ?>
			<?php endif; ?>
		</div>

		<div id="hero-dots"></div>
        <div class="logo clearHeader">
            <a href="<?=get_home_url();?>"class="logo-groot is-active">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="logo-img"/>
            </a>
        </div>

</section>
