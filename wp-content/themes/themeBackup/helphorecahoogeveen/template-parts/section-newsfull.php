<?php
	$args = array(
		'post_type' 		=> 'post',
		'posts_per_page' 	=> '9',
		'post_status'		=> 'publish'
	);

	$news = new WP_Query( $args );
?>

<?php if ( $news->have_posts() ) : ?>
	<section class="s-news full">
		<div class="container">
			<div class="items">
				<?php while ( $news->have_posts() ) : $news->the_post(); ?>
					<?php get_template_part('template-parts/partials/card', 'blog'); ?>
				<?php endwhile; ?>

				<?php /*if (  $news->max_num_pages > 1 ) : ?>
					<button type="button" name="button" class="button--primary blog-loadmore">Meer laden</button>
				<?php endif; */?>
			</div>
		</div>
	</section>
<?php endif; ?>
