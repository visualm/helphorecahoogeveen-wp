<?php
$args = array (
    'post_type'              => array( 'loterijprijzen' ),
    'post_status'            => array( 'publish' ),
    'nopaging'               => true,
    'order'                  => 'DESC',
    'orderby'                => 'title',
);

$loterijprijzen = new WP_Query( $args );
?>
<section class="s-content">
	<div class="container">
		<div class="content">
            <div class="row">
                <div class="col-12 text-center">
                    <h1><?php echo get_the_title(); ?></h1>
                    <hr/>
                </div>

                  <?php  if ( $loterijprijzen->have_posts() ) : ?>
                	 <?php while ( $loterijprijzen->have_posts() ) : $loterijprijzen->the_post(); ?>
                         <div class="col-12 col-md 6 col-lg-3">
                             <div class="partaker text-center">
                                     <?php if ( get_field( 'loterij_prijs_afbeelding' ) ) : ?>
                                    	<a href="" target="_blank" class="text-center">
                                            <span class="partaker-img_container"><img src="<?php the_field( 'loterij_prijs_afbeelding' ); ?>" class="partaker-img" /></span>
                                            <hr/>
                                            <span class="partaker-name text-center"><h3><?php the_field( 'loterij_prijs_naam' ); ?></h3></span>
                                        </a>

                                </div>
                            </div>
                            <?php endif ?>

                	<?php endwhile; ?>
                <?php else : ?>
                	<?php // no rows found ?>
                <?php endif; ?>

            </div>
		</div>

	</div>
</section>
