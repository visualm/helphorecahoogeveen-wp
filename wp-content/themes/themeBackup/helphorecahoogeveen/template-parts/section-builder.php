<?php if ( have_rows( 'builder' ) ):

	$class = "";

	if (is_front_page()):
		$class = "is-odd";
	else :
		$class = "is-even";
	endif;
?>

	<section class="s-builder <?=$class;?>">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1><?php echo get_the_title(); ?></h1>
                    <hr/>
                </div>
            </div>
        </div>

		<?php while ( have_rows( 'builder' ) ) : the_row(); ?>
			<?php if ( get_row_layout() == 'builder_full' ) : ?>
				<div class="content full">
					<div class="container">
					<?php $builder_full_img = get_sub_field( 'builder_full_img' ); ?>
					<?php if ( $builder_full_img ) { ?>
						<div class="block img">
							<figure>
								<img class="lazy" data-src="<?=wp_get_attachment_image_url( $builder_full_img, 'builder-full' );?>" alt="Afbeelding - <?=wp_title();?>">
							</figure>
						</div>
					<?php } ?>
					<?php if (get_sub_field( 'builder_full_text' )): ?>
						<div class="block text">
							<?php the_sub_field( 'builder_full_text' ); ?>

							<?php if (get_sub_field( 'builder_full_button') == '1'): ?>
								<div>
								<a href="<?php the_sub_field( 'builder_full_button_url' ); ?>" class="button--primary has-icon"><?php the_sub_field( 'builder_full_button_text' ); ?><i class="fas fa-chevron-right"></i></a>
								</div>
							<?php endif; ?>

						</div>
					<?php endif; ?>
					<?php if (get_sub_field( 'builder_full_video' )): ?>
						<a class="block img" href="https://youtube.com/watch?v=<?php the_sub_field( 'builder_full_video' ); ?>" target="_blank" data-lity>
							<figure>
								<div class="overlay"><i class="far fa-play-circle"></i></div>
								<img class="lazy" data-src="http://img.youtube.com/vi/<?php the_sub_field( 'builder_full_video' ); ?>/maxresdefault.jpg" alt="Afbeelding - <?=wp_title();?>">
							</figure>
						</a>
					<?php endif; ?>
				</div>
				</div>

			<?php elseif ( get_row_layout() == 'builder_half' ) : ?>
				<div class="content half">
					<div class="container">
						<?php if ( have_rows( 'blok' ) ) : ?>
							<?php while ( have_rows( 'blok' ) ) : the_row(); ?>

								<?php if (get_sub_field('builder_half_type') == 'text'): ?>
									<div class="block text">
										<?php the_sub_field( 'builder_half_text' ); ?>

										<?php if (get_sub_field( 'builder_half_button') == '1'): ?>
											<div>
												<a href="<?php the_sub_field( 'builder_half_button_url' ); ?>" class="button--primary has-icon"><?php the_sub_field( 'builder_half_button_text' ); ?><i class="fas fa-chevron-right"></i></a>
											</div>
										<?php endif; ?>
									</div>
								<?php endif; ?>

								<?php if (get_sub_field('builder_half_type') == 'img'): ?>
									<?php $builder_half_img = get_sub_field( 'builder_half_img' ); ?>
									<?php if ( $builder_half_img ) { ?>
										<figure>
											<img class="lazy" data-src="<?=wp_get_attachment_image_url( $builder_half_img, 'builder-half' );?>" alt="Afbeelding - <?=wp_title();?>">
										</figure>
									<?php } ?>
								<?php endif; ?>

								<?php if (get_sub_field( 'builder_half_video' )): ?>
									<a class="block img" href="https://youtube.com/watch?v=<?php the_sub_field( 'builder_half_video' ); ?>" target="_blank" data-lity>
										<figure>
											<div class="overlay"><i class="far fa-play-circle"></i></div>
											<img class="lazy" data-src="http://img.youtube.com/vi/<?php the_sub_field( 'builder_half_video' ); ?>/maxresdefault.jpg" alt="Afbeelding - <?=wp_title();?>">
										</figure>
									</a>
								<?php endif; ?>

							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>

			<?php elseif ( get_row_layout() == 'builder_third' ) : ?>
				<div class="content third">
					<div class="container">
						<?php if ( have_rows( 'blok' ) ) : ?>
							<?php while ( have_rows( 'blok' ) ) : the_row(); ?>

								<?php if (get_sub_field('builder_third_type') == 'text'): ?>
									<div class="block text">
										<?php the_sub_field( 'builder_third_text' ); ?>

										<?php if (get_sub_field( 'builder_third_button') == '1'): ?>
											<div>
												<a href="<?php the_sub_field( 'builder_third_button_url' ); ?>" class="button--primary has-icon"><?php the_sub_field( 'builder_third_button_text' ); ?><i class="fas fa-chevron-right"></i></a>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (get_sub_field('builder_third_type') == 'img'): ?>
									<?php $builder_third_img = get_sub_field( 'builder_third_img' ); ?>
									<?php if ( $builder_third_img ) { ?>
										<div class="block img">
											<figure>
												<img class="lazy" data-src="<?php echo wp_get_attachment_image_url( $builder_third_img, 'builder-third' ); ?>" alt="Afbeelding - <?=wp_title();?>">
											</figure>
										</div>
									<?php } ?>
								<?php endif; ?>

								<?php if (get_sub_field('builder_third_type') == 'both'): ?>
									<div class="block both">
										<?php $builder_third_img = get_sub_field( 'builder_third_img' ); ?>
										<?php if ( $builder_third_img ) { ?>
											<figure>
												<img class="lazy" data-src="<?php echo wp_get_attachment_image_url( $builder_third_img, 'builder-half' );?>" alt="Afbeelding - <?=wp_title();?>">
											</figure>
										<?php } ?>
									<?php the_sub_field( 'builder_third_text' ); ?>

									<?php if (get_sub_field( 'builder_third_button') == '1'): ?>
										<div>
											<a href="<?php the_sub_field( 'builder_third_button_url' ); ?>" class="button--primary has-icon"><?php the_sub_field( 'builder_third_button_text' ); ?><i class="fas fa-chevron-right"></i></a>
										</div>
									<?php endif; ?>
									</div>
								<?php endif; ?>

								<?php if (get_sub_field('builder_third_type') == 'icon'): ?>
									<div class="block icon">
										<?php the_sub_field( 'builder_third_icon' ); ?>
										<?php the_sub_field( 'builder_third_text' ); ?>
										<?php if (get_sub_field( 'builder_third_button') == '1'): ?>
											<div>
												<a href="<?php the_sub_field( 'builder_third_button_url' ); ?>" class="button--primary has-icon"><?php the_sub_field( 'builder_third_button_text' ); ?><i class="fas fa-chevron-right"></i></a>
											</div>
										<?php endif; ?>
									</div>
								<?php endif; ?>

								<?php if (get_sub_field( 'builder_third_type' ) == 'video'): ?>
									<a class="block img" href="https://youtube.com/watch?v=<?php the_sub_field( 'builder_third_video' ); ?>" target="_blank" data-lity>
										<figure>
											<div class="overlay"><i class="far fa-play-circle"></i></div>
											<img class="lazy" data-src="http://img.youtube.com/vi/<?php the_sub_field( 'builder_third_video' ); ?>/maxresdefault.jpg" alt="Afbeelding - <?=wp_title();?>">
										</figure>
									</a>
								<?php endif; ?>
							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>

			<?php elseif ( get_row_layout() == 'builder_news' ) : ?>

				<?php
				$args = array(
					'post_type' 			=> 'post',
					'status' 				=> 'publish',
					'posts_per_page' 		=> '4',
				);
				?>
				<?php $news = new WP_Query( $args );?>

				<?php if ($news->have_posts()): ?>
					<div class="content news">
						<div class="container">
							<?php while ( $news->have_posts() ) : $news->the_post(); ?>
								<a class="item" href="<?=the_permalink();?>">
									<div class="image">
										<figure>
											<?php if (get_the_post_thumbnail_url()): ?>
												<img class="lazy lazy-bg" data-src="<?= get_the_post_thumbnail_url( $post->ID, 'builder-third' ); ?>" alt="Afbeelding - <?=wp_title();?>">
											<?php else: ?>
												<img class="lazy lazy-bg" data-src="<?=getPlaceholderIMG(290, 150);?>" alt="Afbeelding - <?=wp_title();?>">
											<?php endif; ?>
										</figure>
									</div>
									<div class="inner">
										<h3><?php the_title();?></h3>
										<span class="date"><?=the_date();?></span>
										<?=the_excerpt(); ?>
										<span class="read-more"><?= __('Lees meer...', 'Builder');?></span>
									</div>
								</a>
							<?php endwhile; ?>
						</div>
					</div>
				<?php endif; ?>

				<?php wp_reset_postdata(); ?>

			<?php elseif ( get_row_layout() == 'builder_childpages' ) : ?>

					<?php
						$args = array(
						    'post_type'      => 'page',
						    'posts_per_page' => -1,
						    'post_parent'    => $post->ID,
						    'order'          => 'ASC',
						    'orderby'        => 'menu_order'
						 );


						$parent = new WP_Query( $args );

						if ( $parent->have_posts() ) : ?>
							<div class="content childpages">
								<div class="container">
								    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

										<a href="<?=the_permalink();?>" class="lazy" data-bg="url('<?=the_post_thumbnail_url('builder-news');?>'')">
											<h3><?php the_title();?></h3>
										</a>
								    <?php endwhile; ?>
								</div>
							</div>

						<?php endif; wp_reset_postdata(); ?>

			<?php elseif ( get_row_layout() == 'builder_galery' ) : ?>
				<div class="content gallery">
					<div class="container">
						<?php $builder_galery_content_images = get_sub_field( 'builder_galery_content' ); ?>
						<?php if ( $builder_galery_content_images ) :  ?>
							<?php foreach ( $builder_galery_content_images as $builder_galery_content_image ): ?>
								<a href="<?php echo $builder_galery_content_image['url']; ?>" data-lity>
									<figure>
										<img class="lazy" data-src="<?php echo $builder_galery_content_image['sizes']['thumbnail']; ?>" alt="<?php echo $builder_galery_content_image['alt']; ?>">
									</figure>
								</a>
							<p><?php echo $builder_galery_content_image['caption']; ?></p>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>

		<?php endwhile; ?>
	</section>
<?php endif; ?>
