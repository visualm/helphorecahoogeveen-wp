<?php

	/* Template Name: Prijzenlijst */

	get_header();

	get_template_part('template-parts/section', 'hero');
	get_template_part('template-parts/section', 'breadcrumb');
	get_template_part('template-parts/section', 'loterijprijzen');

	get_footer();
