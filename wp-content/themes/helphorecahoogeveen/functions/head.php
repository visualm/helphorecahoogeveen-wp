<?php
//******************************************************************************
// remove unnecessary header information
function remove_header_info() {
 remove_action('wp_head', 'feed_links_extra', 3);
 remove_action('wp_head', 'feed_links', 2);
 remove_action('wp_head', 'rsd_link');
 remove_action('wp_head', 'wlwmanifest_link');
 remove_action('wp_head', 'wp_generator');
 remove_action('wp_head', 'start_post_rel_link');
 remove_action('wp_head', 'index_rel_link');
 remove_action('wp_head', 'parent_post_rel_link', 10, 0);
 remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); // for WordPress >= 3.0
 remove_action('wp_head', 'print_emoji_detection_script', 7); // Remove Emoji
 remove_action('wp_print_styles', 'print_emoji_styles'); // Remove Emoji JS
 remove_action('wp_head', 'rest_output_link_wp_head', 10);
 remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
}
add_action('init', 'remove_header_info');


//******************************************************************************
// Make pagespeed happy
function remove_style_id($link) {
      return preg_replace("/id='.*-css'/", "", $link);
}
add_filter('style_loader_tag', 'remove_style_id');


//******************************************************************************
// Remove WP embed script
function speed_stop_loading_wp_embed() {
  if (!is_admin()) {
    wp_deregister_script('wp-embed');
  }
}
add_action('init', 'speed_stop_loading_wp_embed');


//******************************************************************************
/* Disable ping back scanner and complete xmlrpc class. */
show_admin_bar( false );


//******************************************************************************
/* Disable ping back scanner and complete xmlrpc class. */
add_filter('wp_xmlrpc_server_class', '__return_false');
add_filter('xmlrpc_enabled', '__return_false');


//******************************************************************************
// remove xpingback header
function remove_x_pingback($headers){
 unset($headers['X-Pingback']);
 return $headers;
}
add_filter('wp_headers', 'remove_x_pingback');


//******************************************************************************
// Disable Feeds
function fb_disable_feed(){
 wp_die(__('No feed available,please visit our <a href="' . get_bloginfo('url') . '">homepage</a>!'));
}

add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'fb_disable_feed', 1);
add_action('do_feed_atom_comments', 'fb_disable_feed', 1);


//******************************************************************************
// remove wp version meta tag and from rss feed
add_filter('the_generator', '__return_false');


//******************************************************************************
// disable WordPress API (JSON)
add_filter('json_enabled', '__return_false');
add_filter('json_jsonp_enabled', '__return_false');

?>
