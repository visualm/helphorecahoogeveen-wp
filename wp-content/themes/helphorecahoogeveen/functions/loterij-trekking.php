<?php
    require_once '../../../../wp-load.php';
    global $wpdb;

    $lottery_tickets = $wpdb->get_results( "SELECT ticket_owner, ticket_number FROM {$wpdb->prefix}lottery_tickets WHERE ticket_winner = 0 ORDER BY RAND() LIMIT 50");
    echo json_encode( $lottery_tickets );
?>
