<?php
//******************************************************************************
// Theme Support
add_theme_support( 'post-thumbnails', array( 'post' , 'page') );// This enabled post-thumbnails
add_theme_support('menus'); // This enables the naviagation menu ability


//******************************************************************************
//add predefined image sizes name, width, height, crop if necessary
add_image_size ( 'logo', 270, 150, true );
add_image_size ( 'news', 390, 250, true );
add_image_size ( 'route', 340, 300, true );

// builder sizes
add_image_size ( 'builder-full', 1920, 1080, true );
add_image_size ( 'builder-half', 665, 445, true );
add_image_size ( 'builder-third', 430, 280, true );
add_image_size ( 'builder-news', 320, 180, true );


//******************************************************************************
// Registering multiple menus
register_nav_menus (
	array(
	'main-menu' => 'Main menu',
	'footer-menu' => 'Footer menu',
	)
);


//******************************************************************************
// Hide admin bar
add_filter('show_admin_bar', '__return_false');


//******************************************************************************
// hide editor
function remove_content_editor()
{
    remove_post_type_support('page', 'editor');
	// remove_post_type_support('post', 'editor');
}
add_action('admin_head', 'remove_content_editor');


//******************************************************************************
// Remove/cleanup paste classes in the editor
add_filter('tiny_mce_before_init', 'customize_tinymce');
function customize_tinymce($in) {
	$in['paste_preprocess'] = "function(pl,o){ o.content = o.content.replace(/p class=\"p[0-9]+\"/g,'p'); o.content = o.content.replace(/span class=\"s[0-9]+\"/g,'span'); }";
	return $in;
}


//******************************************************************************
// Add ACF options page
if( function_exists('acf_add_options_page') ) {
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Thema Settings',
		'menu_title' 	=> 'Thema Settings',
		'menu_slug' 	=> 'thema-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Logos',
		'menu_title' 	=> 'Logos',
		'parent_slug' 	=> $option_page['menu_slug'],
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title' 	=> 'Footer',
		'parent_slug' 	=> $option_page['menu_slug'],
	));

	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Formulieren',
		'menu_title' 	=> 'Formulieren',
		'parent_slug' 	=> $option_page['menu_slug'],
	));
}


//******************************************************************************
// Generate Placeholder image
function getPlaceholderIMG($width, $height){
	$image = "https://dummyimage.com/".$width."x".$height."/ccc/969696";
	return $image;
}


//******************************************************************************
// Load more post ajax loads js
function vm_my_load_more_scripts() {

	$args = array(
		'post_type'  	 => 'post',
		'posts_per_page'  =>  9,
		'post_status'	 => 'published',
	);
	$blog = new WP_Query( $args );

	wp_enqueue_script('jquery');
	wp_register_script( 'my_loadmore', get_template_directory_uri() . '/dist/js/loadmore.min.js');

	wp_localize_script( 'my_loadmore', 'vm_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $blog->max_num_pages - 1,
	) );

 	wp_enqueue_script( 'my_loadmore' );
}

add_action( 'wp_enqueue_scripts', 'vm_my_load_more_scripts' );


//******************************************************************************
// Load more post ajax returns posts
function vm_loadmore_ajax_handler(){

	$args = array(
		'post_type'  	 	=> 'post',
		'posts_per_page'  	=>  2,
		'offset'			=>  2 + $_POST['page'] * 2,
		'post_status'	 	=> 'published',
	);

	query_posts( $args );

	if( have_posts() ) :
		while( have_posts() ): the_post();
			get_template_part( 'template-parts/card', 'blog');
		endwhile;
	endif;
	die;
}

add_action('wp_ajax_loadmore', 'vm_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_loadmore', 'vm_loadmore_ajax_handler');


//******************************************************************************
//add SVG to allowed file uploads
function add_file_types_to_uploads($file_types){

    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );

    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');


//******************************************************************************
// Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


//******************************************************************************
// custom excerpt lenght
function custom_excerpt_length( $length ) {
    return 15;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


//******************************************************************************
// rewrite pagination slug
function re_rewrite_rules() {
	    global $wp_rewrite;
	    $wp_rewrite->pagination_base    = 'pagina';
	    $wp_rewrite->flush_rules();
}
add_action('init', 're_rewrite_rules');

?>
