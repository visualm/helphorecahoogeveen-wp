<?php
add_action( 'woocommerce_payment_complete', 'so_payment_complete' );
function so_payment_complete( $order_id ){

    $order = wc_get_order( $order_id ); // Get the order.
    $user = $order->get_user(); // Set the user.
	$first_name = $order->get_billing_first_name(); // Get the first name of the billing
	$last_name = $order->get_billing_last_name(); // Get the last name of the billing

    // if( $user ){
	// 	// Set the variables
	// 	$ticket_order_id = $order_id;
	// 	$ticket_owner = $first_name . ' ' . $last_name;
	// 	$ticket_owner_email = $order->get_billing_email();
	// 	$ticket_winner = false;
	// 	$ticket_item_count = $order->get_item_count();
    //
	// 	$ticket_number = sprintf('#HHH-%d', $order_id);
    //
	// 	global $wpdb;
	// 	//Give a ticket the number #HHH-000-[amount] and push all ticket data to the database. Example: #HHH-150-2
	// 	for ($i=1; $i < $ticket_item_count; $i++) {
	// 		$ticket_number_single = $ticket_number . '-' . $i;
    //
	// 		$wpdb->insert("dappr_lottery_tickets", array(
	// 		   "ticket_order_id" => $ticket_order_id,
	// 		   "ticket_number" => $ticket_number_single,
	// 		   "ticket_owner" => $ticket_owner,
	// 		   "ticket_owner_email" => $ticket_owner_email,
	// 		   "ticket_winner" => $ticket_winner,
	// 		));
	// 	}
    // }
	// Set the variables
	$ticket_order_id = $order_id;
	$ticket_owner = $first_name . ' ' . $last_name;
	$ticket_owner_email = $order->get_billing_email();
	$ticket_winner = false;
	$ticket_item_count = $order->get_item_count();

	$ticket_number = sprintf('#HHH-%d', $order_id);

	global $wpdb;
	//Give a ticket the number #HHH-000-[amount] and push all ticket data to the database. Example: #HHH-150-2
	for ($i=0; $i < $ticket_item_count; $i++) {
		$ticket_number_single = $ticket_number . '-' . $i;

		$wpdb->insert("dappr_lottery_tickets", array(
		   "ticket_order_id" => $ticket_order_id,
		   "ticket_number" => $ticket_number_single,
		   "ticket_owner" => $ticket_owner,
		   "ticket_owner_email" => $ticket_owner_email,
		   "ticket_winner" => $ticket_winner,
		));
	}
}

function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );


// function getTicketnumbers($ticket_order_id) {
//     include_once ROOT_FOLDER . '/wp-load.php';
//     global $wpdb;
//
//     $sql = "SELECT * FROM dappr_lottery_tickets WHERE ticket_order_id = '$ticket_order_id' ORDER BY ticket_number ASC";
//     $results = $wpdb->get_results($sql);
//
//         foreach( $results as $result ) {
//             echo 'Ticket nummer: ' . $result->ticket_number . '<br />';
//         }
// }
?>
