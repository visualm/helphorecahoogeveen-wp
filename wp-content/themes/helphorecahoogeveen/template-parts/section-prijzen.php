
	<section class="s-prijzenslider">
        
		<div id="slider-prijzen">

            <?php if ( have_rows( 'prijzen' ) ) : ?>
            	<?php while ( have_rows( 'prijzen' ) ) : the_row(); ?>
            		<?php if ( have_rows( 'prijs' ) ) : ?>

                			<?php while ( have_rows( 'prijs' ) ) : the_row(); ?>
				                <?php if ( get_sub_field( 'prijs_img' ) ) : ?>
                                    <div class="slide">
                					   <img src="<?php the_sub_field( 'prijs_img' ); ?>" />
                                    </div>
                				<?php endif; ?>
                			<?php endwhile; ?>

            		<?php else : ?>
            			<?php // no rows found ?>
            		<?php endif; ?>
            	<?php endwhile; ?>
            <?php endif; ?>

		</div>
	</section>
