
<?php if ( function_exists('yoast_breadcrumb') ) : ?>
	<section class="s-breadcrumbs">
		<div class="container">
			<div class="breadcrumbs">
				<?php yoast_breadcrumb();?>
			</div>
		</div>
	</section>
<?php endif; ?>
