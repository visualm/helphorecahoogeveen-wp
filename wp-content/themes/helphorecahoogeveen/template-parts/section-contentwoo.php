<section class="s-content loterij">
    <div class="container">
        <div class="row" style="width: 100%;">
            <div class="col-12 text-center">
                <h1><?php echo get_the_title(); ?></h1>
                <hr/>
            </div>
        </div>
    </div>
	<div class="container">
		<div class="content">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>

				<?php the_content(); ?>
				
			<?php endwhile; endif; ?>
		</div>

	</div>
</section>
