<header class="header mobile">

	<div class="logo logo-groot is-active">
		<?php $logo_id = get_field('logo_light', 'options');?>
		<img class="lazy" data-src="<?= wp_get_attachment_image_url($logo_id, 'full');?>" alt="Logo - <?=wp_title();?>">
	</div>

	<div class="logo logo-klein">
		<?php $logo_id_small = get_field('logo_dark', 'options');?>
		<img class="lazy" data-src="<?= wp_get_attachment_image_url($logo_id_small, 'full');?>" alt="Logo - <?=wp_title();?>">
	</div>

	<div class="hamburger">
		<span>MENU</span>
		<div></div>
		<div></div>
		<div></div>
	</div>


	<div class="nav-overlay">
		<div class="inner">
			<nav class="nav-mobile">
				<?php wp_nav_menu( array(
					'theme_location' => 'main-menu'
				)); ?>
			</nav>

			<div class="socials">
				<div class="item">
					<a href="<?=the_permalink(326);?>"><i class="fas fa-phone"></i></a>
				</div>
				<div class="item">
					<a href="<?=get_field('facebook', 'options');?>" target="_blank"><i class="fas fa-envelope"></i></a>
				</div>

				<div class="item">
					<a href="<?=get_field('linkedin', 'options');?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
				</div>
			</div>
		</div>
	</div>

</header>
