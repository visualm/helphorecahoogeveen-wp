
<?php
$args = array (
    'post_type'              => array( 'prijzen' ),
    'post_status'            => array( 'publish' ),
    'nopaging'               => true,
    'order'                  => 'DESC',
    'orderby'                => 'title',
);

$prijzen = new WP_Query( $args );
?>

<section class="s-content">
	<div class="container">
		<div class="content">
            <div class="row">
                <div class="col-12 text-center">
                    <h1><?php echo get_the_title(); ?></h1>
                    <hr/>
                </div>

                  <?php  if ( $prijzen->have_posts() ) : ?>
                	 <?php while ( $prijzen->have_posts() ) : $prijzen->the_post(); ?>
                         <div class="col-12 col-md 6 col-lg-3">
                             <div class="partaker text-center">
                                     <?php if ( get_field( 'logo_afbeelding' ) ) : ?>
                                    	<a href="<?php the_field( 'logo_link' );  ?>" target="_blank" class="text-center">
                                            <span class="partaker-img_container"><img src="<?php the_field( 'logo_afbeelding' ); ?>" class="partaker-img" /></span>
                                            <span class="partaker-name text-center"><h3><?php the_field( 'logo_naam' ); ?></h3></span>
                                        </a>

                                </div>
                            </div>
                            <?php endif ?>

                	<?php endwhile; ?>
                <?php else : ?>
                	<?php // no rows found ?>
                <?php endif; ?>

            </div>
		</div>

	</div>
</section>
