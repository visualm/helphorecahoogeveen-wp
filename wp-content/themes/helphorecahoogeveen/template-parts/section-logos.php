
<?php
$args = array (
    'post_type'              => array( 'logos' ),
    'post_status'            => array( 'publish' ),
    'nopaging'               => true,
    'order'                  => 'DESC',
    'orderby'                => 'title',
);

$logos = new WP_Query( $args );
?>

<section class="s-content">
	<div class="container">
		<div class="content">
            <div class="row">
                <div class="col-12 text-center">
                    <h1><?php echo get_the_title(); ?></h1>
                    <hr/>
                    <?php get_template_part('template-parts/partials/builder', 'full');?>
                </div>

                  <?php  if ( $logos->have_posts() ) : ?>
                	 <?php while ( $logos->have_posts() ) : $logos->the_post(); ?>
                         <div class="col-12 col-md 6 col-lg-3 ">
                             <div class="partaker text-center logo ">
                                     <?php if ( get_field( 'logo_afbeelding' ) ) : ?>
                                    	<a href="<?php the_field( 'logo_link' );  ?>" target="_blank" class="text-center">
                                            <span class="partaker-img_container"><img src="<?php the_field( 'logo_afbeelding' ); ?>" class="partaker-img" /></span>
                                            <span class="partaker-name text-center"><h3><?php the_field( 'logo_naam' ); ?></h3></span>
                                        </a>

                                </div>
                            </div>
                            <?php endif ?>

                	<?php endwhile; ?>
                <?php else : ?>
                	<?php // no rows found ?>
                <?php endif; ?>

            </div>
		</div>

	</div>
</section>

<section class="thebutton">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="thebutton_container">
                    <button type="button" class="get_prize">
                        <a href="<?php echo get_permalink(82); ?>">
                            Koop hier je loten
                            <img src="<?php echo get_template_directory_uri(); ?>/img/tickets.svg" class="ticket-icon"/>
                        </a>
                    </button>
                    <span>10,- euro per lot  <span class="yellow">meer loten is meer kans!</span></span>
                </div>
            </div>
        </div>
    </div>
</section>
