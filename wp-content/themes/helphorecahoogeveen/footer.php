<?php $phone = preg_replace('/\s+/', '', get_field("phone","options")); ?>

<footer class="footer">

</footer>

<div id="copyright">
	<div class="container">
		<div class="column">
			<a href="<?=get_home_url();?>"class="logo-groot is-active">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="logo-img"/>
			</a>
			<!--<a href="<?=get_bloginfo('url');?>"><footer>Copyright &copy; <?php echo date('Y'); ?> <?=get_bloginfo('name');?> </footer></a>-->
		</div>

		<div class="column text-center">
			<nav class="nav-footer">
				<?php wp_nav_menu( array(
					'theme_location' => 'footer-menu',
					'depth' => '1',
				)); ?>
			</nav>
		</div>

		<div class="column">
			<div class="organizers">
				<img src="<?php echo get_template_directory_uri(); ?>/img/mulderij.png" class="logo-mulderij"/>
				<img src="<?php echo get_template_directory_uri(); ?>/img/dappr.png" class="logo-dappr"/>
			</div>
			<!--<a href="<?=get_bloginfo('url');?>"><footer>Copyright &copy; <?php echo date('Y'); ?> <?=get_bloginfo('name');?> </footer></a>-->
		</div>

	</div>
</div>

<!-- scripts -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
	"@type": "PostalAddress",
	"streetAddress": "<?=get_field("address","options");?>",
	"addressLocality": "<?=get_field("zipcode","options");?>",
	"addressRegion": "<?=get_field("city","options");?>"
  },
  "name": "<?=get_bloginfo('name');?>","telephone": "<?=$phone;?>",
  "email": "<?=get_field("email","options");?>"
}
</script>
<script>
  var themeURL = '<?=get_template_directory_uri(); ?>';
</script>
<script type="text/javascript">
[
  'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
  '<?=autoVer('/dist/js/bootstrap.min.js');?>',
  '<?=autoVer('/dist/js/bootstrap.bundle.min.js');?>',
  '<?=autoVer('/dist/js/loadmore.min.js');?>',
  '<?=autoVer('/dist/js/slick.min.js');?>',
  '<?=autoVer('/dist/js/lity.min.js');?>',
  '<?=autoVer('/dist/js/lazyload.min.js');?>',
  '<?=autoVer('/dist/js/main.min.js');?>',
  ].forEach(function(src) {
  var script = document.createElement('script');
  script.src = src;
  script.async = false;
  document.body.appendChild(script);
});
</script>

<?php wp_footer();?>
</body>
</html>
