<?php
require_once (TEMPLATEPATH . '/functions/head.php'); // declare output related to the head
require_once (TEMPLATEPATH . '/functions/init.php'); // initiates stuff for theme
require_once (TEMPLATEPATH . '/functions/theme-secure.php'); // wordpress/theme security
require_once (TEMPLATEPATH . '/functions/theme-support.php');// declare output related to theme options
require_once (TEMPLATEPATH . '/functions/helpers.php'); // add custom helpers
require_once (TEMPLATEPATH . '/functions/filters.php'); // declare content filters
require_once (TEMPLATEPATH . '/functions/custom-fields.php'); // declare content filters
require_once (TEMPLATEPATH . '/functions/custom-posttype.php'); // theme posttypes /admin
require_once (TEMPLATEPATH . '/functions/admin.php'); // theme options /admin
require_once (TEMPLATEPATH . '/functions/lottery-functions.php'); // theme options /admin
add_theme_support( 'post-thumbnails', array( 'post', 'page', 'movie', 'product' ) );

// add_theme_support('woocommerce');

/**
* Remove all possible fields
 **/
function wc_remove_checkout_fields( $fields ) {

    // Billing fields
    unset( $fields['billing']['billing_address_2'] );

    // Shipping fields
    unset( $fields['shipping']['shipping_company'] );
    unset( $fields['shipping']['shipping_phone'] );
    unset( $fields['shipping']['shipping_state'] );
    unset( $fields['shipping']['shipping_first_name'] );
    unset( $fields['shipping']['shipping_last_name'] );
    unset( $fields['shipping']['shipping_address_1'] );
    unset( $fields['shipping']['shipping_address_2'] );
    unset( $fields['shipping']['shipping_city'] );
    unset( $fields['shipping']['shipping_postcode'] );

    // Order fields
    unset( $fields['order']['order_comments'] );

    return $fields;
}
add_filter( 'woocommerce_checkout_fields', 'wc_remove_checkout_fields' );
