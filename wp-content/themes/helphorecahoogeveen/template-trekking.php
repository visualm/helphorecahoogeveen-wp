<?php

	/* Template Name: trekking */
  get_header();
  // require_once (TEMPLATEPATH . '/functions/loterij-trekking.php'); // Get rows for the lottery drawing
  $counter = 0;
?>

<div class="wrapper">
  <div class="machine">
  <div class="wrap">
    <div class="column">

      <div id="machine1" class="optionContainer">

      </div>
    </div>

    <div class="row">
      <div class="mashupButton">
        <button id="randomizeButton" class="button"><span>Start!</span></button>
      </div>
    </div>
    <div class="row">
      <div id="results">

      </div>
    </div>
  </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/plugins/jquery.slotmachine.min.js"></script>



<script>
$(document).ready(function() {
    $(document).on('click', '#againButton', function(e){

        location.reload();
    });
    $(document).on('click', '#randomizeButton', function(e){

        // e.preventDefault();
        getTicketNumbers();
    });


    // hall tickets op
    function getTicketNumbers()
    {
        // ajax request haalt alle t ickets op die nog niet hebben gewonen
        $.ajax({
            url: '<?php echo get_template_directory_uri(); ?>/functions/loterij-trekking.php',
            type: "POST",
            success: function(data) {
                // bouw html op, buildHMLT()
                var rows = JSON.parse(data);
                var counter = 0;
                // $('.slotMachineContainer').empty();
                var html = '';
                $.each(rows, function(key,val){
                    html += '<span class="option option-' + counter + '" data-name="' + val.ticket_number + '"><h1>' + val.ticket_owner + ' ' + val.ticket_number + '</h1></span>';
                    counter++;
                });

                $("#machine1").html(html);

                // destroySlotMachine();
                initSlotMachine();
                $('#randomizeButton').html('Opnieuw!').attr('id', 'againButton');
            }
        })
    }

    function initSlotMachine()
    {
        var machine1 = $("#machine1").slotMachine({
          active: 0,
          // delay: 200
        });
        // console.log('af');
        $("#results").css('color', 'white').text("");
        console.log('voor de animatie');
        machine1.shuffle(15, onComplete);
    }

    // ajax call udpate functione

    function onComplete(active) {
        // console.log(active);
        // console.log(this.element);

        // var winner = $(this.element).find('.option-' + active);
        // var winnerTicket = winner.data('name');
        // console.log(winner);
        // console.log(winnerTicket);

        // this.element.empty();


        console.log(this);
        switch (this.element[0].id) {
          case "machine1":
            $("#machine1Result").text(this.active);
            results = getMachineResult($('#machine1'), this.active);

            console.log(results);
            var classCounter = '.option-' + this.active;
            var wonTicketNumber = $('.option-' + this.active).attr('data-name');
            console.log(wonTicketNumber);
            console.log('.option-' + this.active);
            console.log($('.option-' + this.active));
            var theme = '<?php echo get_template_directory_uri(); ?>/functions/loterij-winnaar.php';
            var data = {
                action: 'my_action',
                ticket: wonTicketNumber
            }

            jQuery.post(theme, data, function(response) {
                console.log(response);
            })
            break;
        }
        $("#results").text(results);
    }

    function getMachineResult(i_jqMachine, i_iActive){
      return i_jqMachine.find('span.option > span').eq(i_iActive + 1).text();
    }
});


//   $(document).ready(function() {
//   var machine1 = $("#machine1").slotMachine({
//     active: 0,
//     delay: 200
//   });
//
//
//     var results;
//
//
//     function onReroll() {
//         var theme = '<?php echo get_template_directory_uri(); ?>/functions/loterij-trekking.php';
//
//         jQuery.post(theme, function(response) {
//             var rows = JSON.parse(response);
//             var counter = 0;
//
//             $('.slotMachineContainer').empty();
//             $.each(rows, function(key,val){
//                 $('.slotMachineContainer').append('<span class="option option-' + counter + '" data-name="' + val.ticket_number + '"><h1>' + val.ticket_owner + ' ' + val.ticket_number + '</h1></span>');
//                 counter++;
//             })
//         });
//     }
//
//
    // function getMachineResult(i_jqMachine, i_iActive){
    //   return i_jqMachine.find('span.option > span').eq(i_iActive + 1).text();
    // }
    //
    // $("#randomizeButton").click(function() {
    //     results = [];
    //     onReroll();
    //     $("#results").css('color', 'white').text("");
    //     machine1.shuffle(20, onComplete);
    // });
// });

</script>
