<?php

	get_header();

	get_template_part('template-parts/section', 'hero');
	get_template_part('template-parts/section', 'breadcrumb');
	get_template_part('template-parts/section', 'builder');
	get_template_part('template-parts/section', 'cta');
	get_template_part('template-parts/section', 'logoslider'); ?>

    <section class="thebutton">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="thebutton_container">
                        <button type="button" class="get_prize">
                            <a href="<?php echo get_permalink(82); ?>">
                                Koop hier je loten
                                <img src="<?php echo get_template_directory_uri(); ?>/img/tickets.svg" class="ticket-icon"/>
                            </a>
                        </button>
                        <span>10,- euro per lot  <span class="yellow">meer loten is meer kans!</span></span>
                    </div>
                </div>
            </div>
        </div>
    </section>



<?php	get_footer(); ?>
