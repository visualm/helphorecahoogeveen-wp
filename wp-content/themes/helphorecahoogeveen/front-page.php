<?php

	get_header();

	get_template_part('template-parts/section', 'hero-big');
    ?>
    <section class="intro">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 offset-lg-3 text-center">
                    <?php the_field( 'intro_tekst' ); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="thebutton">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="thebutton_container">
                        <button type="button" class="get_prize">
							<a href="<?php echo get_permalink(82); ?>">
								Koop hier je loten
								<img src="<?php echo get_template_directory_uri(); ?>/img/tickets.svg" class="ticket-icon"/>
							</a>
						</button>
                        <span>10,- euro per lot  <span class="yellow">meer loten is meer kans!</span></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="explain">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 offset-lg-3 text-center">
                    <?php the_field( 'uitleg_tekst' ); ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/mulderij.png" class="logo-mulderij"/>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/dappr.png" class="logo-dappr"/>
                </div>
            </div>
        </div>
    </section>


    <?php get_template_part('template-parts/partials/builder', 'full'); ?>
    <?php get_template_part('template-parts/section', 'prijzen'); ?>


	<?php get_footer(); ?>
