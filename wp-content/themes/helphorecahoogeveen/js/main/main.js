$( document ).ready(function() {

	// *************************************************************************
	// add class on scroll to stick header
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if ( !$('body').hasClass( "noscroll" ) ) {
			if(scroll >= 40) {
				$(".header").addClass("scrolled");
				$(".logo-groot").removeClass("is-active");
				$(".logo-klein").addClass("is-active");
			} else {
				$(".header").removeClass("scrolled");
				$(".logo-groot").addClass("is-active");
				$(".logo-klein").removeClass("is-active");
			}
		}
	});

	// also execute on window enter
	var scroll = $(window).scrollTop();

	if ( !$('body').hasClass( "noscroll" ) ) {
		if(scroll >= 50) {
			$(".header").addClass("scrolled");
			$(".logo-groot").removeClass("is-active");
		} else {
			$(".header").removeClass("scrolled");
			$(".logo-groot").addClass("is-active");
		}
	}


	// *************************************************************************
	// Nav menu toggle
	$('.hamburger').on( "click", function() {
		$('.nav-overlay').fadeToggle( "fast", "linear" );
		$('body').toggleClass( "noscroll" );
		$('html').toggleClass( "noscroll" );
		$('.hamburger').toggleClass("is-active");

		if ( $(".header").hasClass( "scrolled" ) ) {
			$(".header").toggleClass("scrolled");
			$(".logo-groot").addClass("is-active");
			$(".logo-klein").removeClass("is-active");
		}
	});


	// *************************************************************************
	// init lazyload
	var myLazyLoad = new LazyLoad({
	    elements_selector: ".lazy"
	});


	// *************************************************************************
	// mobile Nav menu toggle
	$('.nav-mobile .menu-item-has-children').after().click(function() {
		$(this).find('ul.sub-menu').slideToggle();
	});


	// *************************************************************************
	// Slider hero
	$('#slider-hero').slick({
		infinite: false,
		rows: 0,
		slide: '.slide',
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
  		autoplaySpeed: 6000,
		dots: true,
		appendDots: '#hero-dots',
	});


	// *************************************************************************
	// Slider logos
	$('#slider-logo').slick({
		infinite: true,
		rows: 0,
		slide: '.slide',
		slidesToShow: 7,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 6000,
		dots: false,
		responsive: [
			{
				breakpoint: 1200, //$breakpoint-40
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 1000,//$breakpoint-30
				settings: {
					slidesToShow: 5,
				}
			},
			{
				breakpoint: 750,//$breakpoint-20
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 600,//$breakpoint-10
				settings: {
					slidesToShow: 2,
				}
			},
		]
	});

    // *************************************************************************
	// Slider prijzen
	$('#slider-prijzen').slick({
		infinite: true,
		rows: 0,
		slide: '.slide',
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 3500,
		dots: true,

	});

    // *************************************************************************
    // scroll

    $(function() {
        //caches a jQuery object containing the header element
        var header = $(".clearHeader");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 50) {
                header.removeClass('clearHeader').addClass("darkHeader");
            } else {
                header.removeClass("darkHeader").addClass('clearHeader');
            }
        });
    });


	// *************************************************************************
	// Dynamic ajax forms
	$('.dynamic-ajax-form').submit(function(e) {
	e.preventDefault();

	// Get the form
	var currentForm = this;

	// Disable the submit button
	var submitBtn = $(this).find(':submit');
	submitBtn.attr('disabled', true);
	submitBtn.addClass('disabled');

	// validation
	var proceed = true;
	var erroredInputs = [];
	var inputs = $(".dynamic-ajax-form .required");
	for (var i = 0, len = inputs.length; i < len; i++) {
		input = inputs[i];
		if ($(input).hasClass('error')) {
			$(input).removeClass('error');
		}

		// check if a value was entered for text input
		if(
			$(input).attr('type') == 'text' ||
			$(input).attr('type') == 'textarea' ||
			$(input).attr('type') == 'email' ||
			$(input).attr('type') == 'tel' ||
			$(input).attr('type') == 'number' ||
			$(input).is('textarea')
		) {
			if ($(input).val().length < 1) {
				$(input).addClass('error');
				erroredInputs.push($(input).attr('name'));
				proceed = false;
				// console.log(input);
			}
		}

		// check invalid email
		var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if ($(input).attr("type") == "email" && !email_reg.test($(input).val().trim())) {
			$(input).addClass('error');
			erroredInputs.push($(input).attr('name'));
			proceed = false;
		}

		// voorwarden input validatie
		if($('#voorwaarden').length) {
			if (!$('#voorwaarden').prop('checked')) {
				$('.voorwaarden-error').addClass('errored').html('U moet onze algemene voorwaarden accepteren.');
				proceed = false;
				erroredInputs.push('Voorwaarden not accepted');
			}
			else {
				$('.voorwaarden-error').removeClass('errored').html('');
			}
		}
	}

	if (!proceed) {
		var output = '<p class="error"><strong>Validatiefouten opgetreden.</strong><br> Controleer en pas de velden aan en klik daarna nogmaals op verzenden.</div>';
		$('#contact_results').html(output).slideDown();

		// Enable the submit button
		submitBtn.removeAttr('disabled');
		submitBtn.removeClass('disabled');

		//console.log('The following inputs were errored: ', erroredInputs);

		return false;
	}
	$('#contact_results').hide();

	// Send the request
	var actionURL = $(this).attr('action');
	var formMethod = $(this).attr('method');

	var formData = new FormData(this);

	$.ajax({
		url: actionURL,
		method: formMethod,
		data: formData,
		processData: false,
		contentType: false,
		dataType: 'json',
		success: function(received) {

			if(received.type == 'error') {
				$('#contact_results').show();
				$('#contact_results').html('<p class="error">' + received.text + '</p>');

				$("#" + received.target).addClass("error");
			}
			else if(received.type == 'message') {
				$('#contact_results').show();
				$('#contact_results').html('<p class="success">' + received.text + '</p>');
				$(currentForm).slideUp();
				$('html,body').animate({scrollTop:$('#formscroll').offset().top - 200}, 500);

				//conversion($(currentForm).attr('id'));
			}
			else if(received.type == 'redirect') {
				window.location.href = received.redirectURL;
				return;
			}
			// Enable the submit button
			submitBtn.removeAttr('disabled');
			submitBtn.removeClass('disabled');
		},
		error: function(received) {
			//console.log('ajax request to ' + actionURL + ' failed');
			//console.log(received);

			// Enable the submit button
			submitBtn.removeAttr('disabled');
			submitBtn.removeClass('disabled');
		}
	});
});

});
