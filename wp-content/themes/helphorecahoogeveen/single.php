<?php

	get_header();

	get_template_part('template-parts/section', 'hero');
	get_template_part('template-parts/section', 'breadcrumb');
	get_template_part('template-parts/section', 'content'); ?>


<?php	get_footer(); ?>
